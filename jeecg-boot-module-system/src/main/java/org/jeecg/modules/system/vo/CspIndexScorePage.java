package org.jeecg.modules.system.vo;

import java.util.List;
import org.jeecg.modules.system.entity.CspIndexScore;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 打分表
 * @Author: jeecg-boot
 * @Date:   2021-11-06
 * @Version: V1.0
 */
@Data
@ApiModel(value="csp_index_scorePage对象", description="打分表")
public class CspIndexScorePage {

	/**主键*/
	@ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**得分*/
	@Excel(name = "得分", width = 15)
	@ApiModelProperty(value = "得分")
    private java.lang.String score;
	/**指标*/
	@Excel(name = "指标", width = 15)
	@ApiModelProperty(value = "指标")
    private java.lang.String indicator;
	/**评分作者*/
	@Excel(name = "评分作者", width = 15)
	@ApiModelProperty(value = "评分作者")
    private java.lang.String user;
	/**年度*/
	@Excel(name = "年度", width = 15)
	@ApiModelProperty(value = "年度")
    private java.lang.String year;
	/**季度*/
	@Excel(name = "季度", width = 15)
	@ApiModelProperty(value = "季度")
    private java.lang.String quarter;


}
