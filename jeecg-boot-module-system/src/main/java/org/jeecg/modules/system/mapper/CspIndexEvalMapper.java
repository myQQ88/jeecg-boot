package org.jeecg.modules.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.system.entity.CspIndexEval;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 指标评价
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
public interface CspIndexEvalMapper extends BaseMapper<CspIndexEval> {

}
