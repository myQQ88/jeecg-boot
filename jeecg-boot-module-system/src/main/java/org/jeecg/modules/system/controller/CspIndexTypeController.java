package org.jeecg.modules.system.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.system.entity.CspIndexType;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 指标类型
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
@Api(tags="指标类型")
@RestController
@RequestMapping("/system/cspIndexType")
@Slf4j
public class CspIndexTypeController extends JeecgController<CspIndexType, ICspIndexTypeService> {
	@Autowired
	private ICspIndexTypeService cspIndexTypeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param cspIndexType
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "指标类型-分页列表查询")
	@ApiOperation(value="指标类型-分页列表查询", notes="指标类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CspIndexType cspIndexType,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CspIndexType> queryWrapper = QueryGenerator.initQueryWrapper(cspIndexType, req.getParameterMap());
		Page<CspIndexType> page = new Page<CspIndexType>(pageNo, pageSize);
		IPage<CspIndexType> pageList = cspIndexTypeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param cspIndexType
	 * @return
	 */
	@AutoLog(value = "指标类型-添加")
	@ApiOperation(value="指标类型-添加", notes="指标类型-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CspIndexType cspIndexType) {
		cspIndexTypeService.save(cspIndexType);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param cspIndexType
	 * @return
	 */
	@AutoLog(value = "指标类型-编辑")
	@ApiOperation(value="指标类型-编辑", notes="指标类型-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CspIndexType cspIndexType) {
		cspIndexTypeService.updateById(cspIndexType);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "指标类型-通过id删除")
	@ApiOperation(value="指标类型-通过id删除", notes="指标类型-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		cspIndexTypeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "指标类型-批量删除")
	@ApiOperation(value="指标类型-批量删除", notes="指标类型-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.cspIndexTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "指标类型-通过id查询")
	@ApiOperation(value="指标类型-通过id查询", notes="指标类型-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CspIndexType cspIndexType = cspIndexTypeService.getById(id);
		if(cspIndexType==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(cspIndexType);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param cspIndexType
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CspIndexType cspIndexType) {
        return super.exportXls(request, cspIndexType, CspIndexType.class, "指标类型");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CspIndexType.class);
    }

}
