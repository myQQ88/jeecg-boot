package org.jeecg.modules.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.system.entity.CspIndexScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 打分表
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
public interface CspIndexScoreMapper extends BaseMapper<CspIndexScore> {

}
