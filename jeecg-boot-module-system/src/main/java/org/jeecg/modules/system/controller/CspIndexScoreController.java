package org.jeecg.modules.system.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.system.entity.CspIndexScore;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 打分表
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
@Api(tags="打分表")
@RestController
@RequestMapping("/system/cspIndexScore")
@Slf4j
public class CspIndexScoreController extends JeecgController<CspIndexScore, ICspIndexScoreService> {
	@Autowired
	private ICspIndexScoreService cspIndexScoreService;
	
	/**
	 * 分页列表查询
	 *
	 * @param cspIndexScore
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "打分表-分页列表查询")
	@ApiOperation(value="打分表-分页列表查询", notes="打分表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CspIndexScore cspIndexScore,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CspIndexScore> queryWrapper = QueryGenerator.initQueryWrapper(cspIndexScore, req.getParameterMap());
		Page<CspIndexScore> page = new Page<CspIndexScore>(pageNo, pageSize);
		IPage<CspIndexScore> pageList = cspIndexScoreService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param cspIndexScore
	 * @return
	 */
	@AutoLog(value = "打分表-添加")
	@ApiOperation(value="打分表-添加", notes="打分表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CspIndexScore cspIndexScore) {
		cspIndexScoreService.save(cspIndexScore);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param cspIndexScore
	 * @return
	 */
	@AutoLog(value = "打分表-编辑")
	@ApiOperation(value="打分表-编辑", notes="打分表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CspIndexScore cspIndexScore) {
		cspIndexScoreService.updateById(cspIndexScore);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "打分表-通过id删除")
	@ApiOperation(value="打分表-通过id删除", notes="打分表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		cspIndexScoreService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "打分表-批量删除")
	@ApiOperation(value="打分表-批量删除", notes="打分表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.cspIndexScoreService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "打分表-通过id查询")
	@ApiOperation(value="打分表-通过id查询", notes="打分表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CspIndexScore cspIndexScore = cspIndexScoreService.getById(id);
		if(cspIndexScore==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(cspIndexScore);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param cspIndexScore
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CspIndexScore cspIndexScore) {
        return super.exportXls(request, cspIndexScore, CspIndexScore.class, "打分表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CspIndexScore.class);
    }

}
