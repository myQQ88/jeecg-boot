package org.jeecg.modules.indicator.service.impl;

import org.jeecg.modules.indicator.entity.CspIndexEval;
import org.jeecg.modules.indicator.mapper.CspIndexEvalMapper;
import org.jeecg.modules.indicator.service.ICspIndexEvalService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 指标评价
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
@Service
public class CspIndexEvalServiceImpl extends ServiceImpl<CspIndexEvalMapper, CspIndexEval> implements ICspIndexEvalService {

}
