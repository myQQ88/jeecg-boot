package org.jeecg.modules.indicator.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.indicator.entity.CspIndexType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 指标类型
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
public interface CspIndexTypeMapper extends BaseMapper<CspIndexType> {

}
