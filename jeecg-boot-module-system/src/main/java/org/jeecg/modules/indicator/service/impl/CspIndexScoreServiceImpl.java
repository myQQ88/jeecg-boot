package org.jeecg.modules.indicator.service.impl;

import org.jeecg.modules.indicator.entity.CspIndexScore;
import org.jeecg.modules.indicator.mapper.CspIndexScoreMapper;
import org.jeecg.modules.indicator.service.ICspIndexScoreService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 打分表
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
@Service
public class CspIndexScoreServiceImpl extends ServiceImpl<CspIndexScoreMapper, CspIndexScore> implements ICspIndexScoreService {

}
