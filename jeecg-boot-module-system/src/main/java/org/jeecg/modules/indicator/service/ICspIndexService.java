package org.jeecg.modules.indicator.service;

import org.jeecg.modules.indicator.entity.CspIndex;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 指标表
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
public interface ICspIndexService extends IService<CspIndex> {

}
