package org.jeecg.modules.indicator.service;

import org.jeecg.modules.indicator.entity.CspIndexScore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 打分表
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
public interface ICspIndexScoreService extends IService<CspIndexScore> {

}
