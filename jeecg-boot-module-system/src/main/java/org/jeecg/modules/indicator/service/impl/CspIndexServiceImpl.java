package org.jeecg.modules.indicator.service.impl;

import org.jeecg.modules.indicator.entity.CspIndex;
import org.jeecg.modules.indicator.mapper.CspIndexMapper;
import org.jeecg.modules.indicator.service.ICspIndexService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 指标表
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
@Service
public class CspIndexServiceImpl extends ServiceImpl<CspIndexMapper, CspIndex> implements ICspIndexService {

}
