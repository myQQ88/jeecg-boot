package org.jeecg.modules.indicator.service.impl;

import org.jeecg.modules.indicator.entity.CspIndexType;
import org.jeecg.modules.indicator.mapper.CspIndexTypeMapper;
import org.jeecg.modules.indicator.service.ICspIndexTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 指标类型
 * @Author: jeecg-boot
 * @Date:   2021-11-07
 * @Version: V1.0
 */
@Service
public class CspIndexTypeServiceImpl extends ServiceImpl<CspIndexTypeMapper, CspIndexType> implements ICspIndexTypeService {

}
