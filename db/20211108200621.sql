/*
MySQL Backup
Database: jeecg-boot
Backup Time: 2021-11-08 20:06:21
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `jeecg-boot`.`csp_index`;
DROP TABLE IF EXISTS `jeecg-boot`.`csp_index_eval`;
DROP TABLE IF EXISTS `jeecg-boot`.`csp_index_score`;
DROP TABLE IF EXISTS `jeecg-boot`.`csp_index_type`;
CREATE TABLE `csp_index` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `title` varchar(64) NOT NULL COMMENT '指标名称',
  `description` varchar(100) DEFAULT NULL COMMENT '指标描述',
  `indtype` varchar(36) DEFAULT NULL COMMENT '指标类型',
  `indevel` varchar(36) DEFAULT NULL COMMENT '指标评价',
  `datafrom` varchar(64) DEFAULT NULL COMMENT '数据来源',
  `target` varchar(64) DEFAULT NULL COMMENT '目标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `csp_index_eval` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `title` varchar(32) NOT NULL COMMENT '评价名称',
  `description` varchar(100) DEFAULT NULL COMMENT '评价描述',
  `rate` int(11) DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `csp_index_score` (
  `id` varchar(36) NOT NULL,
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `score` varchar(32) DEFAULT NULL COMMENT '得分',
  `indicator` varchar(32) DEFAULT NULL COMMENT '指标',
  `user` varchar(32) DEFAULT NULL COMMENT '评委',
  `year` varchar(32) DEFAULT NULL COMMENT '年度',
  `quarter` varchar(32) DEFAULT NULL COMMENT '季度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
CREATE TABLE `csp_index_type` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `create_by` varchar(50) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `update_by` varchar(50) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新日期',
  `sys_org_code` varchar(64) DEFAULT NULL COMMENT '所属部门',
  `title` varchar(32) NOT NULL COMMENT '指标类型名称',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `rate` int(10) DEFAULT NULL COMMENT '分值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
BEGIN;
LOCK TABLES `jeecg-boot`.`csp_index` WRITE;
DELETE FROM `jeecg-boot`.`csp_index`;
INSERT INTO `jeecg-boot`.`csp_index` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`sys_org_code`,`title`,`description`,`indtype`,`indevel`,`datafrom`,`target`) VALUES ('1457189887471120385', 'admin', '2021-11-07 11:35:17', 'admin', '2021-11-07 21:18:50', 'A01', 'test', '测试专勇', '通用指标', '大家评价咯', '手工维护', '完成啊'),('1457336630833704961', 'admin', '2021-11-07 21:18:23', NULL, NULL, 'A01', '大', '打', '罚分', ' 发', '发呆', '打分');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `jeecg-boot`.`csp_index_eval` WRITE;
DELETE FROM `jeecg-boot`.`csp_index_eval`;
INSERT INTO `jeecg-boot`.`csp_index_eval` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`sys_org_code`,`title`,`description`,`rate`) VALUES ('1457279655760490497', 'admin', '2021-11-07 17:32:00', NULL, NULL, 'A01', '评价', '评价', 80);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `jeecg-boot`.`csp_index_score` WRITE;
DELETE FROM `jeecg-boot`.`csp_index_score`;
INSERT INTO `jeecg-boot`.`csp_index_score` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`sys_org_code`,`score`,`indicator`,`user`,`year`,`quarter`) VALUES ('1457190588125409282', 'admin', '2021-11-07 11:38:04', NULL, NULL, 'A01', '测试', '地点', '的', '22', '大');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `jeecg-boot`.`csp_index_type` WRITE;
DELETE FROM `jeecg-boot`.`csp_index_type`;
INSERT INTO `jeecg-boot`.`csp_index_type` (`id`,`create_by`,`create_time`,`update_by`,`update_time`,`sys_org_code`,`title`,`description`,`rate`) VALUES ('1457279595731611650', 'admin', '2021-11-07 17:31:45', 'admin', '2021-11-08 17:17:49', 'A01', '通用指标', '通用指标', NULL),('1457638530955300865', 'admin', '2021-11-08 17:18:02', NULL, NULL, 'A01', '测试', '实施', 11);
UNLOCK TABLES;
COMMIT;
